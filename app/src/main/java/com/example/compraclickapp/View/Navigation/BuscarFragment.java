package com.example.compraclickapp.View.Navigation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.compraclickapp.Model.Producto;
import com.example.compraclickapp.R;
import com.example.compraclickapp.ViewModel.Adapters.BuscarProductosAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class BuscarFragment extends Fragment {


    private List<Producto> productoList;
    private BuscarProductosAdapter adapterProductosBuscar;
    private RecyclerView recyclerViewProductosBuscar;

    public BuscarFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buscar, container, false);
        recyclerViewProductosBuscar = view.findViewById(R.id.recyclerViewCardProductsBuscar);
        productoList = listaDeCardsProductos();

        adapterProductosBuscar = new BuscarProductosAdapter(productoList);
        recyclerViewProductosBuscar.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerViewProductosBuscar.setAdapter(adapterProductosBuscar);

        return view;
    }

    public List<Producto> listaDeCardsProductos() {
        List<Producto> lists = new ArrayList<>();
        lists.add(new Producto("Computadora", "$3600", "Jonathan", 8, "Huauchinango", R.drawable.home_slide1, "La descripcion"));
        lists.add(new Producto("Lapiz", "$850", "Jonathan", 8, "Huauchinango", R.drawable.home_slide2, "La descripcion"));
        lists.add(new Producto("AlgoMas", "$998", "Jonathan", 8, "Huauchinango", R.drawable.home_slide3, "La descripcion"));
        lists.add(new Producto("Computadora", "$3600", "Jonathan", 8, "Huauchinango", R.drawable.home_slide1, "La descripcion"));
        lists.add(new Producto("Lapiz", "$850", "Jonathan", 8, "Huauchinango", R.drawable.home_slide2, "La descripcion"));
        lists.add(new Producto("AlgoMas", "$998", "Jonathan", 8, "Huauchinango", R.drawable.home_slide3, "La descripcion"));
        lists.add(new Producto("Computadora", "$3600", "Jonathan", 8, "Huauchinango", R.drawable.home_slide1, "La descripcion"));
        lists.add(new Producto("Lapiz", "$850", "Jonathan", 8, "Huauchinango", R.drawable.home_slide2, "La descripcion"));
        lists.add(new Producto("AlgoMas", "$998", "Jonathan", 8, "Huauchinango", R.drawable.home_slide3, "La descripcion"));
        lists.add(new Producto("Computadora", "$3600", "Jonathan", 8, "Huauchinango", R.drawable.home_slide1, "La descripcion"));
        lists.add(new Producto("Lapiz", "$850", "Jonathan", 8, "Huauchinango", R.drawable.home_slide2, "La descripcion"));
        lists.add(new Producto("AlgoMas", "$998", "Jonathan", 8, "Huauchinango", R.drawable.home_slide3, "La descripcion"));



        return lists;
    }
}
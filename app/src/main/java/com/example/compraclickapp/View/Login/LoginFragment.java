package com.example.compraclickapp.View.Login;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.compraclickapp.Model.ApiAdapters.LoginApiAdapter;
import com.example.compraclickapp.Model.Responses.LoginResponse;
import com.example.compraclickapp.R;
import com.example.compraclickapp.View.Navigation.Home;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginFragment extends Fragment implements Callback<LoginResponse> {


    public LoginFragment() {
        // Required empty public constructor
    }

    Button btnLogin, btnSignup;
    EditText txtEmail, txtPass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        btnLogin = view.findViewById(R.id.btnLogin);
        btnSignup = view.findViewById(R.id.btnSignup);
        txtEmail = view.findViewById(R.id.etUser);
        txtPass = view.findViewById(R.id.etPass);

        btnLogin.setOnClickListener(v -> {
            String e = txtEmail.getText().toString();
            String p = txtPass.getText().toString();
            Call<LoginResponse> call =  LoginApiAdapter.getApiService().getLogin(e,p);
            call.enqueue(this);


        });

        btnSignup.setOnClickListener(v -> {
            try {
                FragmentManager frm =  getFragmentManager();
                FragmentTransaction ft = frm.beginTransaction();
                ft.replace(R.id.contentLogin, new SignupFragment()).commit();
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
        });


        return view;
    }

    @Override
    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
        if(response.isSuccessful()){
            Intent intent = new Intent(getContext(), Home.class);
            startActivity(intent);
        }else{
            Toast.makeText(getContext(), "Usuario no regitrado", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Call<LoginResponse> call, Throwable t) {

    }
}
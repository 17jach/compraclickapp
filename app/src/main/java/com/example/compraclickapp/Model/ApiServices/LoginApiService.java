package com.example.compraclickapp.Model.ApiServices;

import com.example.compraclickapp.Model.Responses.LoginResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LoginApiService {
    @GET("login/autenticar")
    Call<LoginResponse> getLogin(
            @Query("email") String email,
            @Query("pass") String password
    );
}

package com.example.compraclickapp.ViewModel.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.compraclickapp.Model.Producto;
import com.example.compraclickapp.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HomeProductosAdapter extends RecyclerView.Adapter<HomeProductosAdapter.ViewHolder> {


    private List<Producto> productoList;

    public HomeProductosAdapter(List<Producto> productoList) {
        this.productoList = productoList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_productos_home, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getTvPrecio().setText(productoList.get(position).getPrecioProducto());
        holder.getImgProducto().setImageResource(productoList.get(position).getImgRuta());
        holder.getBtnVerProducto().setOnClickListener(v->{
            verProducto(v.getContext());
        });
    }

    private void verProducto(Context context) {
        Toast.makeText(context, "Quieres ver un producto", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return productoList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvPrecio;
        private final ImageView imgProducto;
        private final Button btnVerProducto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvPrecio = itemView.findViewById(R.id.tvPrecio);
            imgProducto = itemView.findViewById(R.id.imgProductosHome);
            btnVerProducto = itemView.findViewById(R.id.btnVerProducto);
        }

        public ImageView getImgProducto(){
            return imgProducto;
        }

        public TextView getTvPrecio() {
            return tvPrecio;
        }

        public Button getBtnVerProducto() {return btnVerProducto;}
    }
}

package com.example.compraclickapp.View.Navigation;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.compraclickapp.R;
import com.example.compraclickapp.View.Login.MainActivity;


public class MiCuentaFragment extends Fragment {


    private ImageView imgSalir;
    private TextView tvSalir;
    private Button btnVender;


    public MiCuentaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mi_cuenta, container, false);
        imgSalir = view.findViewById(R.id.imgSalir);
        tvSalir = view.findViewById(R.id.tvSalir);
        btnVender = view.findViewById(R.id.btnVender);

        btnVender.setOnClickListener(v->{
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.content, new VenderFragment()).commit();
        });

        tvSalir.setOnClickListener(v->{
            salir();
        });

        imgSalir.setOnClickListener(v->{
            salir();
        });

        return view;
    }

    public void salir(){
        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
    }
}
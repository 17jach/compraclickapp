package com.example.compraclickapp.View.Navigation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.compraclickapp.Model.HomeCardSlider;
import com.example.compraclickapp.Model.Producto;
import com.example.compraclickapp.R;
import com.example.compraclickapp.ViewModel.Adapters.HomeCardSliderAdapter;
import com.example.compraclickapp.ViewModel.Adapters.HomeProductosAdapter;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment {


    private List<HomeCardSlider> cardSliders;
    private RecyclerView recyclerViewSliderHome;
    private HomeCardSliderAdapter adapterSlider;

    private List<Producto> productos;
    private RecyclerView recyclerViewProductosHome;
    private HomeProductosAdapter adapterProductosHome;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerViewSliderHome = view.findViewById(R.id.recyclerViewSliderHome);
        recyclerViewProductosHome = view.findViewById(R.id.recyclerViewCardProducts);


        cardSliders = listaDeCardsSlider();
        productos = listaDeCardsProductos();

        adapterSlider = new HomeCardSliderAdapter(cardSliders);
        adapterProductosHome = new HomeProductosAdapter(productos);

        recyclerViewSliderHome.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewProductosHome.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));

        recyclerViewSliderHome.setAdapter(adapterSlider);
        recyclerViewProductosHome.setAdapter(adapterProductosHome);


        return view;
    }

    public List<Producto> listaDeCardsProductos() {
        List<Producto> lists = new ArrayList<>();
        lists.add(new Producto("Computadora", "$3600", "Jonathan", 8, "Huauchinango", R.drawable.home_slide1, "La descripcion"));
        lists.add(new Producto("Lapiz", "$850", "Jonathan", 8, "Huauchinango", R.drawable.home_slide2, "La descripcion"));
        lists.add(new Producto("AlgoMas", "$998", "Jonathan", 8, "Huauchinango", R.drawable.home_slide3, "La descripcion"));

        return lists;
    }

    public List<HomeCardSlider> listaDeCardsSlider() {
        List<HomeCardSlider> lists = new ArrayList<>();
        lists.add(new HomeCardSlider(R.drawable.home_slide1));
        lists.add(new HomeCardSlider(R.drawable.home_slide2));
        lists.add(new HomeCardSlider(R.drawable.home_slide3));

        return lists;

    }


}
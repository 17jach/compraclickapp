package com.example.compraclickapp.ViewModel.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.compraclickapp.Model.Producto;
import com.example.compraclickapp.R;
import com.example.compraclickapp.View.Navigation.ComprarFragment;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

public class BuscarProductosAdapter extends RecyclerView.Adapter<BuscarProductosAdapter.ViewHolder> {

    private List<Producto> productoList;

    public BuscarProductosAdapter(List<Producto> productoList) {
        this.productoList = productoList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_productos_buscar, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getTvTituloProducto().setText(productoList.get(position).getTituloProducto());
        holder.getTvPrecio().setText(productoList.get(position).getPrecioProducto());
        holder.getImgProducto().setImageResource(productoList.get(position).getImgRuta());
        holder.getBtnComprar().setOnClickListener(v -> {
            final AppCompatActivity activity = (AppCompatActivity) v.getContext();  // así obtienes el context

            try {
                FragmentManager fm = activity.getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.content, new ComprarFragment(productoList.get(position))).commit();
            }catch (Exception e){
                System.out.println(e.getMessage());
            }

        });

    }

    @Override
    public int getItemCount() {
        return productoList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTituloProducto;
        private TextView tvPrecio;
        private ImageView imgProducto;
        private Button btnComprar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTituloProducto = itemView.findViewById(R.id.tvTituloProducto);
            tvPrecio = itemView.findViewById(R.id.tvPrecioBuscar);
            btnComprar = itemView.findViewById(R.id.btnComprarBuscar);
            imgProducto = itemView.findViewById(R.id.imgProductosBuscar);
        }

        public TextView getTvTituloProducto() {
            return tvTituloProducto;
        }

        public TextView getTvPrecio() {
            return tvPrecio;
        }

        public Button getBtnComprar() {
            return btnComprar;
        }

        public ImageView getImgProducto() {
            return imgProducto;
        }
    }


}

package com.example.compraclickapp.View.Login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.example.compraclickapp.R;

public class MainActivity extends AppCompatActivity {


    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frameLayout = findViewById(R.id.contentLogin);

        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(frameLayout.getId(), new LoginFragment()).commit();






    }

}
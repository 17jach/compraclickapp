package com.example.compraclickapp.Model;

public class HomeCardSlider {

    private int ruta;

    public int getRuta() {
        return ruta;
    }

    public void setRuta(int ruta) {
        this.ruta = ruta;
    }

    public HomeCardSlider(int ruta) {
        this.ruta = ruta;
    }
}

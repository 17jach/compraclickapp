package com.example.compraclickapp.View.Login;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.compraclickapp.R;


public class SignupFragment extends Fragment {



    Button btnContinuar;
    public SignupFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        btnContinuar = view.findViewById(R.id.btnContinue);

        btnContinuar.setOnClickListener(v -> {
            try {
                FragmentManager frm =  getFragmentManager();
                FragmentTransaction ft = frm.beginTransaction();
                ft.replace(R.id.contentLogin, new SignedFragment()).commit();
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
        });

        return view;
    }
}
package com.example.compraclickapp.Model;

public class Producto {
    private String tituloProducto;
    private String precioProducto;
    private String vendedorNombre;
    private int existentes;
    private String lugarOrigen;
    private int imgRuta;
    private String descripcion;

    public Producto(String tituloProducto, String precioProducto, String vendedorNombre, int existentes, String lugarOrigen, int imgRuta, String descripcion) {
        this.tituloProducto = tituloProducto;
        this.precioProducto = precioProducto;
        this.vendedorNombre = vendedorNombre;
        this.existentes = existentes;
        this.lugarOrigen = lugarOrigen;
        this.imgRuta = imgRuta;
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        descripcion = descripcion;
    }

    public int getImgRuta() {
        return imgRuta;
    }

    public void setImgRuta(int imgRuta) {
        this.imgRuta = imgRuta;
    }

    public String getTituloProducto() {
        return tituloProducto;
    }

    public void setTituloProducto(String tituloProducto) {
        this.tituloProducto = tituloProducto;
    }

    public String getPrecioProducto() {
        return precioProducto;
    }

    public void setPrecioProducto(String precioProducto) {
        this.precioProducto = precioProducto;
    }

    public String getVendedorNombre() {
        return vendedorNombre;
    }

    public void setVendedorNombre(String vendedorNombre) {
        this.vendedorNombre = vendedorNombre;
    }

    public int getExistentes() {
        return existentes;
    }

    public void setExistentes(int existentes) {
        this.existentes = existentes;
    }

    public String getLugarOrigen() {
        return lugarOrigen;
    }

    public void setLugarOrigen(String lugarOrigen) {
        this.lugarOrigen = lugarOrigen;
    }


}

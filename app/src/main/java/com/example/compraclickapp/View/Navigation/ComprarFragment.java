package com.example.compraclickapp.View.Navigation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.compraclickapp.Model.Producto;
import com.example.compraclickapp.R;


public class ComprarFragment extends Fragment {


    private Producto producto;
    private Button btnComprar, btnVolver;
    private TextView tvTitulo, tvDecripcion;
    private ImageView imgProducto;

    public ComprarFragment(Producto producto) {
        this.producto=producto;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comprar, container, false);

        btnComprar = view.findViewById(R.id.btnComprarComprar);
        btnVolver = view.findViewById(R.id.btnVolverComprar);
        tvTitulo = view.findViewById(R.id.tvTituloProductoComprar);
        tvDecripcion = view.findViewById(R.id.tvDescripcionComprar);
        imgProducto = view.findViewById(R.id.imgProductosComprar);

        tvTitulo.setText(producto.getTituloProducto());
        tvDecripcion.setText(producto.getDescripcion());
        imgProducto.setImageResource(producto.getImgRuta());



        btnVolver.setOnClickListener(v->{
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.content, new BuscarFragment()).commit();
        });
        btnComprar.setOnClickListener(v->{
            Toast.makeText(getContext(), "Quieres comprar " + producto.getTituloProducto(), Toast.LENGTH_SHORT).show();
        });


        return view;
    }
}
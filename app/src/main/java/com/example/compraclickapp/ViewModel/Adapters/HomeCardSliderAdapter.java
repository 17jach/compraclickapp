package com.example.compraclickapp.ViewModel.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.compraclickapp.Model.HomeCardSlider;
import com.example.compraclickapp.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HomeCardSliderAdapter extends RecyclerView.Adapter<HomeCardSliderAdapter.ViewHolder> {

    private List<HomeCardSlider> cardSliders;


    public HomeCardSliderAdapter(List<HomeCardSlider> cardSliders) {
        this.cardSliders = cardSliders;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_slider_home, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getImgSlide().setImageResource(cardSliders.get(position).getRuta());
    }

    @Override
    public int getItemCount() {
        return cardSliders.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imgSlide;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgSlide = (ImageView)itemView.findViewById(R.id.imgSlider);
        }
        public ImageView getImgSlide(){
            return imgSlide;
        }

    }
}
